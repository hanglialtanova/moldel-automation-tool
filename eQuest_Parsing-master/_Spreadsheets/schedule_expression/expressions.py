'''
pre-processor to be implemented between
wizard and detailed mode (detailed mode must be
selected in "Mode").

Currently need to have only one window per zone or you'll
get net negative areas.



WISH LIST:
    
    - Option for Windows Process (additionally strip multiple windows and doors)
    
    - Add Single Floor option to spaces_zones_strip, window process, and maybe others so
      you can better integrate a floor merge
    
    - Add option to NOT do Windows Process
    
'''

import sys
pydir = "C:/PythonScripts/eQuest"
if pydir not in sys.path:
    sys.path.insert(0,pydir)

import re
import pandas as pd
import xlwings as xw
import inp_parse.objects as objects


def spaces_zones_strip(inpfile,floor=None):
    '''
    removes values shown
    in 'strip_from_spaces' and 'strip_from_zones'.
    these lists can be modified.
    '''

    with open(inpfile, encoding="Latin1") as f:
        flist = f.readlines()
     
    spaces = objects.get(flist, "SPACE", parent=False)
    zones = objects.get(flist, "ZONE", parent=False)   
   
    strip_from_spaces = ["AREA/PERSON", 
                         "PEOPLE-SCHEDULE", 
                         "LIGHTING-SCHEDUL",
                         "EQUIPMENT-W/AREA",
                         "EQUIP-SCHEDULE",
                         "LIGHTING-SCHEDUL",
                         "EQUIP-SCHEDUL",
                         "LIGHTING-W/AREA",
                         "C-ACTIVITY-DESC"]
        
    strip_from_zones = ["HEAT-TEMP-SCH", 
                        "FLOW/AREA", 
                        "OA-FLOW/PER", 
                        "PEOPLE-SCHEDULE", 
                        "COOL-TEMP-SCH", 
                        "DESIGN-HEAT-T", 
                        "DESIGN-COOL-T"]
    
    for s in strip_from_spaces:
        spaces = objects.stripvals(spaces,s)
    
    for z in strip_from_zones:
        zones = objects.stripvals(zones,z)  
    
    plenumlist = []
    #change plenums to zero loads, zone-type = plenum
    for key, value in spaces.items():
        if "Plnm" in key:
            plenumlist.append(key)
            value['ZONE-TYPE'] = "PLENUM"
            value["LIGHTING-W/AREA"] = 0
            value["AREA/PERSON"] = 9999
            value["EQUIPMENT-W/AREA"] = 0
      
    for p in plenumlist:
        spaces[p].pop("Z")
        spaces[p].pop("HEIGHT")   
    
    for key, value in zones.items():
        if "Pl Zn" in key:
            value['TYPE'] = "PLENUM"
    spaces_stripped = objects.writeinp(flist,inpfile,spaces,write=False)
    zones_stripped = objects.writeinp(spaces_stripped,inpfile,zones,write=False)    
        
    return zones_stripped




def window_process(flist,inpfile):
    '''
    strips windows, adds params'
    '''
      
    #add params:
    globparamnew = ["""\n PARAMETER\n "Nwwr" = 0.3  ..\n PARAMETER\n "Ewwr" = 0.3 ..\n PARAMETER\n "Swwr" = 0.3 ..\n PARAMETER\n "Wwwr" = 0.3 .."""]
    for num, f in enumerate(flist):
        if "Global Parameters" in f:
            paramnum = num + 1
            
        if "Floors / Spaces / Walls / Windows / Doors" in f:
            defaultnum = num
            break
                
    windowdefaults = spacepastenew = ["""\nSET-DEFAULT FOR WINDOW \n HEIGHT = \n {if (MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360) >= 0\n && MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360)<60 ) then \n #PA("Nwwr")*#P3("FLOOR-HEIGHT")\n*#P("WIDTH")/#L("WIDTH") \nelse if(MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360) >= 300 \n&& MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360)<360 )\n then #PA("Nwwr")*#P3("FLOOR-HEIGHT")\n*#P("WIDTH")/#L("WIDTH") \nelse if(MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360) >= 60\n&& MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360)<150) \n then #PA("Ewwr")*#P3("FLOOR-HEIGHT")\n*#P("WIDTH")/#L("WIDTH")  \nelse if(MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360)  >= 150 \n&& MOD(#p3("AZIMUTH")+#p2("AZIMUTH")+#p("AZIMUTH"),360)<210 ) \nthen  #PA("Swwr")*#P3("FLOOR-HEIGHT")\n*#P("WIDTH")/#L("WIDTH") \nelse  #PA("Wwwr")*#P3("FLOOR-HEIGHT")\n*#P("WIDTH")/#L("WIDTH") \nendif \nendif \nendif \nendif\n\n}\n WIDTH ={#p("WIDTH")\n}\n..\n"""]
    windows = objects.get(flist,"WINDOW")    

    strip_from_windows = ["WIDTH",
                          "HEIGHT",
                          "FRAME-WIDTH",
                          "FRAME-CONDUCT",
                          "X",
                          "Y"]
   
    for w in strip_from_windows:
        windows = objects.stripvals(windows, w, key_save = "GLASS-TYPE")

    flist = flist[0:paramnum] + globparamnew + flist[paramnum:defaultnum] + windowdefaults + flist[defaultnum:]
    windows_stripped = objects.writeinp(flist,inpfile,windows,write=False)
    
        
    return windows_stripped



def param_inject(newinplist,xlfile,inpout):
    '''
    equest pre-processor to be used
    between wizard and detailed mode.
    reads excel file and injects
    global parameters as well as all 
    schedules.
    IMPORTANT: this can only be used on a   
                    model that has freshly
                    entered detailed mode.
                    results will be unpredictable
                    to unusable if used on 
                    a model with significant 
                    user input  
               
    '''
    
    #pull columns from excel and make lists, parsing from dataframe to eliminate extraneous characters
    globparams = pd.read_excel(xlfile, sheet_name="Loads", usecols="AV:AV", header=None)
    globparams = globparams.values.T.tolist()
    globparamslist = []
    for a in globparams:
        for b in a:
            globparamslist.append(b)

    spcdefault = pd.read_excel(xlfile, sheet_name="Loads", usecols="AW:AW", header=None)
    spcdefault = spcdefault.values.T.tolist()
    spcdefaultlist = []
    for a in spcdefault:
        for b in a:
            spcdefaultlist.append(b)

    zndefault = pd.read_excel(xlfile, sheet_name="Loads", usecols="AX:AX", header=None)
    zndefault = zndefault.values.T.tolist()
    zndefaultlist = []
    for a in zndefault:
        for b in a:
            zndefaultlist.append(b)

    # def schedule_inject(xlfile,sheet,cols)
    allscheds = pd.read_excel(xlfile, sheet_name="AllSchedules", usecols = "C:C", header = None)
    allscheds = allscheds.values.T.tolist()
    allschedlist = []
    for a in allscheds:
        for b in a:
            allschedlist.append(b)        
            
    #inject parameters and defaults into inp file
    gpfind = "$              Global Parameters"
    globparamstring = ''.join(globparamslist)
    newinplist = [w.replace(gpfind, (gpfind+"\n"+globparamstring)) for w in newinplist]

    spcfind = "Spaces / Walls / Windows / Doors      **"
    spcdefaultstring = ''.join(spcdefaultlist)
    newinplist = [w.replace(spcfind, (spcfind+"\n"+spcdefaultstring)) for w in newinplist]

    znfind = "HVAC Systems / Zones                  **"
    zndefaultstring = ''.join(zndefaultlist)
    newinplist = [w.replace(znfind, (znfind+"\n"+zndefaultstring)) for w in newinplist]

    schfind = "Day Schedules"
    schstring = ''.join(allschedlist)
    newinplist = [w.replace(schfind, (schfind+"\n"+schstring)) for w in newinplist]

    return newinplist
    


def expressionbounce(inputfile,excelfile="Schedule Matrix and Expressions.xlsx",inpout="PreProcess.inp",windowstrip = True):
    '''
    TOP LEVEL USER FUNCTION:
    
    combination of spaces_zones_strip
    and globaparam_inject.
    for use only immediately after
    mode: "Detailed" has been selected.
    Otherwise, pass spaces_zones_strip
    with a floor specified.
    '''
    a = spaces_zones_strip(inputfile)

    b  = param_inject(a,excelfile,inpout)
    c = window_process(b,inputfile)
    with open(inpout, 'w') as new:
        new.writelines(["%s\n" % item for item in c])
        
    print (inputfile +
           "\nhas been read and processed via\n" +
           excelfile + 
           ". the following file has been created:\n" +
           inpout  )
#           "\nPress Enter to continue")
#    input()
    return b
    




#example functionality
inputfile = 'C:/Users/msweeney/Documents/eQUEST 3-64 Projects/Project 6/project 6.inp'
excelfile = "Schedule Matrix and Expressions.xlsx"
outputfile = 'C:/Users/msweeney/Documents/eQUEST 3-64 Projects/Project 6/project 6_2.inp'


e = expressionbounce(inputfile,excelfile,outputfile)



