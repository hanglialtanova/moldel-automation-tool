import pandas as pd

def schedules_export(xlfile="Schedule Matrix and Expressions.xlsx",
                     fileout='schedules_out.txt',
                     sheet="AllSchedules",
                     cols="C:C"):
    
    '''bounces schedules only for manual editing / insertion int equest model.
    can also use the "Import INP" file option within eQuest'''
    
    allscheds = pd.read_excel(xlfile, sheet_name=sheet, usecols=cols, header=None)
    allscheds = allscheds.values.T.tolist()
    allschedlist = []
    for a in allscheds:
        for b in a:
            allschedlist.append(b)

    #what is this??
    for n in allschedlist:
        n.replace("Audi", "something")

    with open(fileout, 'w') as f:
        for item in allschedlist:
            f.write("%s\n" % item)
            
    dayschcount = 0
    weekschcount = 0
    annschcount = 0
    
    for f in allschedlist:
        if "DAY-SCHEDULE-PD" in f:
            dayschcount += 1
        if "WEEK-SCHEDULE-PD" in f:
            weekschcount += 1
        if " SCHEDULE-PD" in f:
            annschcount += 1
                
    print (str(dayschcount) +  " Day Schedules,\n"+
           str(weekschcount) + " Week Schedules, and \n"+
           str(annschcount) +  " Annual Schedules\n"+    
           "Successfully Generatored within\n" +
           xlfile +
           "\nPress Enter to Continue...")
    input()
    
if __name__ == "__main__":
    schedules_export()
