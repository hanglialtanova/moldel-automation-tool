
import pandas as pd
import xlwings as xw

def makeschedules(xlpath='Schedule Matrix and Expressions.xlsx'):
    '''
    intermediary step using excel file. takes all combinations of
    schedules input in first set of tabs and pivots them to
    day schedules, week schedules, and annual schedules.
    read excel file instructions for more info.
    '''
    wb = xw.Book(xlpath)

    daysht = wb.sheets['PyDays']
    wksht = wb.sheets['PyWeeks']
    yrsht = wb.sheets['PyYear']

    xlfile = xlpath

    #days
    spacesets = pd.read_excel(xlfile, sheet_name="dayscheds", index=0)                        
    spacesets.index = spacesets['Space Sets']  
    schedtypes = pd.read_excel(xlfile, sheet_name="schedtypes", index=0)
    schedtypes = schedtypes.dropna(how='all').dropna(axis=1)
    concdf = pd.concat([spacesets, schedtypes], axis=1)
    schedlist = schedtypes.columns.tolist()
    daylist = spacesets.columns.tolist()
    concdflist = concdf.columns.tolist()
    concdflist = concdflist[1:]

    daysets = concdf.melt(id_vars=daylist, value_vars=schedlist)
    daysets = daysets.rename(index=str, columns={"value": "schtype"})
    daysets = daysets.drop(['variable'], axis=1)
    daysets = daysets.melt(id_vars=['Space Sets', 'schtype'])
    daysets = daysets.rename(index=str, columns={"value": "daytype"})
    daysets = daysets.drop(['variable'], axis=1)
    daysets = daysets.sort_values(['Space Sets', 'schtype'], axis=0, ascending=[True, False])
    daysets = daysets.dropna(how='all').dropna(axis=0)

    #weeks
    weeksets = pd.read_excel(xlfile, sheet_name="weekscheds", index=0).dropna(how='all')#.dropna(axis=1)
    weeksets.index = weeksets['Week Sets']
    weeklist = weeksets.columns.tolist()
    weeksets = pd.concat([weeksets, schedtypes], axis=1)
    weeksets = weeksets.melt(id_vars=weeklist).drop(['variable'], axis=1)
    weeksets = weeksets.rename(index=str, columns={"value": "schtype"})
    weeksets = weeksets.melt(id_vars=['Week Sets', 'schtype']).drop(['variable'], axis=1)
    weeksets = weeksets.sort_values(['Week Sets', 'schtype'], axis=0)
    weeksets = weeksets.dropna(how='all').dropna(axis=0)

    #annual
    annsets = schedtypes
    annsets['Annual Sets'] = annsets.index
    annsets = annsets.melt(id_vars=['Annual Sets']).drop(['variable'], axis=1).rename(index=str, columns={"value": "schtype"})
    annsets = annsets.sort_values(['Annual Sets', 'schtype'], axis=0)

    daysht.range('A:D').clear()
    wksht.range('A:D').clear()
    yrsht.range('A:C').clear()

    daysht.range('A5').value = daysets
    wksht.range('A5').value = weeksets
    yrsht.range('A5').value = annsets

    print (str(len(daysets)) +  " Day Schedules,\n"+
           str(len(weeksets)) + " Week Schedules, and \n"+
           str(len(annsets)) +  " Annual Schedules\n"+
           
           "Successfully Generatored within\n" +
           xlfile +
           "\nPress Enter to Continue...")
    input()
if __name__ == "__main__":
    makeschedules()
