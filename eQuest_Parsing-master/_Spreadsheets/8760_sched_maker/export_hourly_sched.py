import pandas as pd

def hourly_sched_export(xlfile="8760 Schedule Maker.xlsx",
                        fileout='8760_export.txt',
                        sheetname="Hourly Data",
                        cols="BM:BM"):
    '''bounces 8760/hourly schedule to hourly schedule in equest format.'''

    allscheds = pd.read_excel(xlfile, sheet_name=sheetname, usecols=cols, header=None)
    allscheds = allscheds.values.T.tolist()
    allschedlist = []
    for a in allscheds:
        for b in a:
            allschedlist.append(b)
    #what is this??
    for n in allschedlist:
        n.replace("Audi", "something")
    with open(fileout, 'w') as f:
        for item in allschedlist:
            f.write("%s\n" % item)
    print ("Hourly Schedule Output successfully bounced from xlfile: \n" +
           xlfile +
           "\nto text file:\n" +
           fileout +
           "\nPress Enter to Continue...")
    input()


if __name__ == "__main__":
    hourly_sched_export()
        