'''
a series of functions allowing user to collect objects in equest
and assign new values to them and also update existing values.

FUNCTIONS TO MAKE: TBD

'''

from collections import OrderedDict
import re
import numpy as np
import pandas as pd
import operator


parentdict = {
    "ZONE":"SYSTEM",
    "SPACE":"FLOOR",
    "EXTERIOR-WALL":"SPACE",
    "INTERIOR-WALL":"SPACE",
    "UNDERGROUND-WALL":"SPACE",
    "WINDOW":"EXTERIOR-WALL",
    "DOOR":"EXTERIOR-WALL"
    }



def openinp(inpread):
    with open(inpread) as f:
        inpfile = f.readlines()
    return inpfile



def get(listname, inptype="", parent=True):
    '''returns nested dictionary, one level deep.
    inptype is type from inp file.
    i.e., 'SYSTEM', 'FLOOR'
    DOESN'T WORK FOR SCHEDULES
    parent argument does not function

    '''
    localparent = 'none'
    topdict = OrderedDict()
    write = False
    bottomdict = OrderedDict()
    for num, line in enumerate(listname):

        #ESTABLISH WHEN TO WRITE
        if "= " + inptype in line:
            write = True
            topkey = re.sub('=.*', '', line).strip().strip("\"")

            #WRITE PARENT (IF APPLICABLE)
            if parent:
                if inptype in parentdict:
                    for x in reversed(listname[0:num]):
                        if "= " + parentdict[inptype] + "  " in x:
                            localparent = re.sub('=.*', '', x).strip().strip("\"")
                            break

        #POPULATE DICTIONARIES
        if write:
            if "=" in line:
                bottomkey = re.sub('=.*', '', line).strip()#.strip("\"")
                bottomval = re.sub('.*=', '', line).strip()#.strip("\"")
                bottomdict[bottomkey] = bottomval

            if ".." in line and parent:
                topdict[topkey] = bottomdict
                bottomdict['parent'] = localparent
                bottomdict = OrderedDict()
                write = False
                
            if ".." in line and not parent:
                topdict[topkey] = bottomdict
                bottomdict = OrderedDict()
                write = False
    
    #dict modifiers for extraneous info for certain inptypes
    if inptype == 'POLYGON':
        topdict = remove(topdict,'SHAPE')
    if inptype == 'SPACE':
        topdict = remove(topdict,'WASTE-HEAT-USE')
      
    #update this with if clause
    try:
        topdict.pop('LOCATION')
    except:
        pass
    return topdict






def getall(listname):
    '''returns nested dictionary, one level deep.
    of all objects. needs to have equest pre-formatting,
    in particular, object name needs to have zero spaces in 
    front of it and object values need to have there spaces
    in front of them

    '''
    topdict = OrderedDict()
    write = False
    bottomdict = OrderedDict()
    for  line in listname:

        #ESTABLISH WHEN TO WRITE
        if "= " in line and line[0:2] !="  ":
            write = True
            topkey = re.sub('=.*', '', line).strip().strip("\"")

        #POPULATE DICTIONARIES
        if write:
            if "=" in line:
                bottomkey = re.sub('=.*', '', line).strip()#.strip("\"")
                bottomval = re.sub('.*=', '', line).strip()#.strip("\"")
                bottomdict[bottomkey] = bottomval

            if ".." in line:
                topdict[topkey] = bottomdict
                bottomdict = OrderedDict()
                write = False
        
    return topdict





def get_sys_zones(inpin):
    '''uses 'get' module and adds a list of zones served by the system.
    returns ordered dictionary of systems
        '''

    inpin = openinp(inpin)
    
    sys_dict = get(inpin,"SYSTEM")
    zndict = get(inpin,"ZONE")
    
    for k, v in zndict.items():
        if v['parent'] in sys_dict:
            
            if "attached_zones" not in sys_dict[v['parent']].keys():
                sys_dict[v['parent']]['attached_zones'] = [k]
                
            else:

                sys_dict[v['parent']]['attached_zones'].append(k)#sysdict[v['parent']]['attached_zones'].append(k)

    return sys_dict



def remove(dictionary,str_remove, str_save=None):
    '''
    removes keys and associated values from an 
    ordereddictionary, likely created using 'get',
    based on str_remove. 
    optional: str_save: remove exception (keep these)   
    
    '''
#    can remove these if no errors. otherwise uncomment.
#    removekeys = []
#    removedict = dictionary
    if str_save:
        removedict = OrderedDict({k:v for k,v in dictionary.items() if str_remove not in k or str_save in k})

    else:
        removedict = OrderedDict({k:v for k,v in dictionary.items() if str_remove not in k})
    return removedict



def stripvals(dictionary, key_remove, key_save=None):#,val_remove):#,str_save='None'):
    
    '''
    strips out keys within all objects
    (i.e. "EQUIP-LATENT")
    
    '''
    
    removekeys = []
    removedict = OrderedDict()#dictionary
    if key_save:
        for key, value in dictionary.items():
            removedict[key] = OrderedDict({k1:v1 for k1,v1 in value.items() if key_remove not in k1 or key_save in k1})
    if not key_save:
         for key, value in dictionary.items():
            removedict[key] = OrderedDict({k1:v1 for k1,v1 in value.items() if key_remove not in k1})
    return removedict



def objectvaluetuple(dictname, innervalue, nullval = None, fromparent=False, parentdict=None):
    '''makes a list of tuples of outervalue / innervalue,
    i.e. "Zn 1", "Unconditioned" from 
    nested ODict created using "get" function

    '''
    if not fromparent:
        odict = OrderedDict()
        for k, v in dictname.items():
            try:
                odict[k] = v[innervalue].strip("\"")
            except:
                odict[k] = nullval
        odict = list(odict.items())
        return odict
    
    if fromparent:
        odict = OrderedDict()
        for k, v in dictname.items():
            try:
                parent = v['parent']
                odict[k] = parentdict[parent][innervalue].strip("\"")
            except:
                odict[k] = nullval
        odict = list(odict.items())
        return odict     
        	


def filterdictbyvalue(dictname,valkey,valvalue):
    '''takes only objects from a given set if they match the value specified.
    e.g. filterdictbyvalue(zones,'TYPE','CONDITIONED')
    yields a dict of dict of all conditioned zones
    '''
    filtlist = {k:v for k, v in dictname.items() if v[valkey] == valvalue}
    return filtlist



def updatedict(dictionary, topkey, bottomkey, value):
    '''
    updates dictionary after using 'get'
    '''
    dictionary[topkey][bottomkey] = value
    return dictionary



def writeinp(listname, newinp, dictname, write=True):
    '''
    define types to update:
    go through list
    if a string matches the type, log the tag matching the dictionary
    if the tag matches a key in the dictionary,
    delete from that string til the next '..'
    insert, at the same place in flist, each key/value pair, except parent, with " = " in between
    '''

    listname = [i for i in listname if len(i) > 1]

    for num, line in enumerate(listname):
        topkey = re.sub('=.*', '', line).strip().strip("\"")
        if topkey != 'LOCATION' and topkey in dictname.keys():
            delete = True
            tlist = []
            for k1, v1 in dictname[topkey].items():

                if 'parent' not in k1 and str(np.nan) not in str(v1):
                    tlist.append(str(k1) + " = " + str(v1))

            tlist.append("   ..")
            listname[num] = [t for t in tlist]

    #remove older elements
    n_list = listname
    delete = False
    for num, line in enumerate(listname):
        if type(line) == list:
            delete = True
        if delete and type(line) != list:
            n_list[num] = "$"
        if "  .." in line:
            delete = False

    flat_list = []
    for n in n_list:
        if type(n) == str:
            flat_list.append(n)
        if type(n) == list:
            for x in n:
                flat_list.append(x)

    flat_list = list(filter(lambda x: x != "$", flat_list))
    flat_list = [f for f in flat_list if len(f) > 2 or "\n" not in f or ".." in f or "}" in f]
    
    
    if write:
        with open(newinp, 'w') as new:
            for item in flat_list:
#                if len(item) <3 :
#                    print (item)
                new.write("%s\n" % item)
#                if type(item) != str:
#                    print (type(item))
#                new.write(item)
#                new.write('\n')
#            new.writelines(["%s\n" % item for item in flat_list])
    
    return flat_list
