'''
a series of functions allowing user to collect objects in equest
and assign new values to them and also update existing values.

FUNCTIONS TO MAKE: TBD

TOP LEVEL FUNCTIONS TO USE: xlmatrixupdate

NEED TO ADD AN "ARE YOU SURE" TYPE THING

NEED TO MAKE ARCHIVE IF OLDINP = NEWINP
'''

from collections import OrderedDict
import re
import numpy as np
import pandas as pd
import operator
import inp_parse.objects as objects
import xlwings as xw

def inp_to_xl(wkbk, sht, bdltype, oldinp,tocsv=False):
    '''
    top level command
    update excel file with matrix from specified input file
    dependent upon writeinp
    needs:
    get, writeinp, update
    if tocsv = True, csv file will be created.
        in this case, "wkbk" will be used as 'csv' file
        and will return an error
    '''

    flist = objects.openinp(oldinp)
    bdldict = objects.get(flist,bdltype)
    for key, value in bdldict.items():
        value.popitem(0)    
    numsystems = len(bdldict)
    bdldf = pd.DataFrame.from_dict(bdldict).transpose()
    
    
    
    if not tocsv:
        if ".xls" not in wkbk:
            print ("ERROR: ARG 'TOCSV=FALSE' PASSED, BUT\nNON-XLS FILE PASSED TO WKBK")
            return "error"
    
        wb = xw.Book(wkbk)
        writesht = wb.sheets[sht]
        
        
        writesht.range('A:ZZ').clear()
        writesht.range('A1').value= bdldf
    #    writesht.range('A:ZZ').options(pd.DataFrame).value
    if tocsv:
        if ".csv" in wkbk:
            
            with open (wkbk, 'a') as f:
                bdldf.to_csv(f, header = True)
        else:
            print ("ERROR: ARG 'TOCSV=TRUE' PASSED, BUT\nNON-CSV FILE PASSED TO WKBK")
            
    return bdldf






def xl_to_inp(wkbk, sheet, bdltype, oldinp, newinp):

    '''
    top level command
    update inp file with matrix from specified excel document
    dependent upon writeinp
    needs:
    get, writeinp, update
    '''
    
    flist = objects.openinp(oldinp)
    
    bdldict = objects.get(flist, bdltype)
    bdlupdate = pd.read_excel(wkbk, sheet_name=sheet)
    
    collist = bdlupdate.columns.tolist()
#    print (collist)
 
    for index, row in bdlupdate.iterrows():
        for num, line in enumerate(row):
            objects.updatedict(bdldict, index, collist[num], line)

    objects.writeinp(flist, newinp, bdldict)
    
    return bdldict       
    




