import glob as gb
import os
import xlwings as xw
import shutil

#os.chdir(r"C:\Users\msweeney\Desktop\MS EM Tools\Python\Python Scripts\Sample SIMs\Sample hsr")


def hsrtocsv():

	hsrfiles = gb.glob('./*.hsr')

	for h in hsrfiles:
		if os.path.isfile(h[:-4] + '.csv'):
			os.remove(h[:-4] + '.csv')

		shutil.copy(h,h[:-4]+".csv")

		wb = xw.Book(h[:-4] + '.csv')
		wb.close()

		
if __name__ == "__main__":
	hsrtocsv()