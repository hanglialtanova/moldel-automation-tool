import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns; sns.set()
import datetime
import itertools
import os
import glob as gb

#read csv
data = pd.read_csv(r"\\Terminalny\projects\_Projects\G150000\G150015-000\Calculations\Mech\Energy Model\eQuest\eQuest Model\July 2018 Models\Proposed\CPL - Baseline Design.hsr",skiprows=9)
data = data[data.columns].astype(float)


#add hour of year, week of year and day of year to df
data['HourofYear'] = range(1,8761)
dayofyear = []
for i in range(1,366):
    dayofyear.append([i] * 24)
    mergeddyofyr = list(itertools.chain(*dayofyear))
data['DayofYear'] = mergeddyofyr

weekofyear = []
for i in range(0,54):
    weekofyear.append([i] * 168)
    mergedwkofyr = list(itertools.chain(*weekofyear))

data['WeekofYear'] = mergedwkofyr[0:8760]

#create heatmap
#print (data.columns)

plotdata = data.columns.tolist()



#fankw = pd.DataFrame['Vent fan end-use energy.1']



for a in data:
    ax1 = data.plot.scatter(x = 'Vent fan end-use energy', y = a)



#help(sns.pairplot(alldata)


#
#datapivot = data.pivot_table(i,'Hour','WeekofYear',aggfunc='mean')
#
#
#f, ax = plt.subplots(figsize=(20, 10))
#ax.set_title(i)
#myheatmap = sns.heatmap(datapivot, linewidths=0, ax=ax)

#save heatmap
#    plt.savefig('whatever.png',dpi=400)




