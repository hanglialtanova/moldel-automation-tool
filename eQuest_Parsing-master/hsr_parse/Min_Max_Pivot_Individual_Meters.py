
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns; sns.set()
import datetime
import itertools
import xlwings as xw
import os
#get_ipython().magic('pylab inline')

os.chdir(r"P:\_Projects\170000\171410-000\D-Design Mgmt\Calc\Energy\Energy Model\Inputs")

#####################################################################3

dataProp = pd.read_csv(r"P:\_Projects\170000\171410-000\D-Design Mgmt\Calc\Energy\Energy Model\eQuest\Individual Meters\Proposed Design\Rutgers Prop - Baseline Design.hsr",skiprows=9)
dataProp = dataProp[dataProp.columns].astype(float)

dataProp['HourofYear'] = range(1,8761)
dayofyear = []
for i in range(1,366):
    dayofyear.append([i] * 24)
    mergeddyofyr = list(itertools.chain(*dayofyear))
dataProp['DayofYear'] = mergeddyofyr



#####################################################################3

dataExist = pd.read_csv(r"P:\_Projects\170000\171410-000\D-Design Mgmt\Calc\Energy\Energy Model\eQuest\Individual Meters\Existing Buildings\Rutgers Existing - Baseline Design.hsr",skiprows=9)
dataExist = dataExist[dataExist.columns].astype(float)

dataExist['HourofYear'] = range(1,8761)
dayofyear = []
for i in range(1,366):
    dayofyear.append([i] * 24)
    mergeddyofyr = list(itertools.chain(*dayofyear))
dataExist['DayofYear'] = mergeddyofyr

#####################################################################3

dataCombElec = dataExist['Total end-use energy'] + dataProp['Total end-use energy']
dataCombHW = dataExist['Total end-use energy.1'] + dataProp['Total end-use energy.1']
dataCombCHW = dataExist['Total end-use energy.2'] + dataProp['Total end-use energy.2']
dataComb = pd.concat([dataProp.DayofYear,dataCombElec,dataCombHW,dataCombCHW],axis=1)

#####################################################################3


elecpivotminProp = dataProp.pivot_table('Total end-use energy','DayofYear',aggfunc='min')
elecpivotmaxProp = dataProp.pivot_table('Total end-use energy','DayofYear',aggfunc='max')
hwpivotminProp = dataProp.pivot_table('Total end-use energy.1','DayofYear',aggfunc='min')
hwpivotmaxProp = dataProp.pivot_table('Total end-use energy.1','DayofYear',aggfunc='max')
chwpivotminProp = dataProp.pivot_table('Total end-use energy.2','DayofYear',aggfunc='min')
chwpivotmaxProp = dataProp.pivot_table('Total end-use energy.2','DayofYear',aggfunc='max')


elecpivotminExist = dataExist.pivot_table('Total end-use energy','DayofYear',aggfunc='min')
elecpivotmaxExist = dataExist.pivot_table('Total end-use energy','DayofYear',aggfunc='max')
hwpivotminExist = dataExist.pivot_table('Total end-use energy.1','DayofYear',aggfunc='min')
hwpivotmaxExist = dataExist.pivot_table('Total end-use energy.1','DayofYear',aggfunc='max')
chwpivotminExist = dataExist.pivot_table('Total end-use energy.2','DayofYear',aggfunc='min')
chwpivotmaxExist = dataExist.pivot_table('Total end-use energy.2','DayofYear',aggfunc='max')

#####################################################################3

elecpivotminComb = dataComb.pivot_table('Total end-use energy','DayofYear',aggfunc='min')
elecpivotmaxComb = dataComb.pivot_table('Total end-use energy','DayofYear',aggfunc='max')
hwpivotminComb = dataComb.pivot_table('Total end-use energy.1','DayofYear',aggfunc='min')
hwpivotmaxComb = dataComb.pivot_table('Total end-use energy.1','DayofYear',aggfunc='max')
chwpivotminComb = dataComb.pivot_table('Total end-use energy.2','DayofYear',aggfunc='min')
chwpivotmaxComb = dataComb.pivot_table('Total end-use energy.2','DayofYear',aggfunc='max')

#####################################################################3


concatDF = pd.concat([elecpivotminProp,elecpivotmaxProp,hwpivotminProp,hwpivotmaxProp,chwpivotminProp,chwpivotmaxProp,elecpivotminExist,elecpivotmaxExist,hwpivotminExist,hwpivotmaxExist,chwpivotminExist,chwpivotmaxExist,elecpivotminComb,elecpivotmaxComb,hwpivotminComb,hwpivotmaxComb,chwpivotminComb,chwpivotmaxComb],axis=1)

concatDF.columns = ['elecpivotminProp','elecpivotmaxProp','hwpivotminProp','hwpivotmaxProp','chwpivotminProp','chwpivotmaxProp','elecpivotminExist','elecpivotmaxExist','hwpivotminExist','hwpivotmaxExist','chwpivotminExist','chwpivotmaxExist','elecpivotminComb','elecpivotmaxComb','hwpivotminComb','hwpivotmaxComb','chwpivotminComb','chwpivotmaxComb']


with open("LoadConcatenate.csv", 'w') as f:
    concatDF.to_csv(f, header = True)        
  
wb = xw.Book("LoadConcatenate.csv")
wb.close()


