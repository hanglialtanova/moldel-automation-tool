'''

Takes all HSR files in a directory and organizes each
block within each according to a set of day / week /
month criteria (i.e. "day / night / weekday / weekend
/ summer / winter"). this is customizable in the "dict"
sections


to-do:

- change seasons to add month / day functionality (i.e.
  "summer starts 5/15")

- totals section? you can easily find it on your own
  by summing any ov the dictionaries but might be
  nice

'''

import os
import glob as gb
import pandas as pd
import xlwings as xw

hsrlist = gb.glob("*.hsr")

daysdict  =   {1:  'night',
               2:  'night',
               3:  'night',
               4:  'night',
               5:  'night',
               6:  'night',
               7:  'day',
               8:  'day',
               9:  'day',
               10: 'day',
               11: 'day',
               12: 'day',
               13: 'day',
               14: 'day',
               15: 'day',
               16: 'day',
               17: 'day',
               18: 'night',
               19: 'night',
               20: 'night',
               21: 'night',
               22: 'night',
               23: 'night',
               24: 'night'
              }

weeksdict =   {1:  'weekday',
               2:  'weekday',
               3:  'weekday',
               4:  'weekday',
               5:  'weekday',
               6:  'weekend',
               7:  'weekend',
               8:  'holiday'
              }

seasonsdict = {1:  'winter',
               2:  'winter',
               3:  'winter',
               4:  'winter',
               5:  'winter',
               6:  'winter',
               7:  'summer',
               8:  'summer',
               9:  'winter',
               10: 'winter',
               11: 'winter',
               12: 'winter'
              }

sumdump = []

#read hsr's
for h in hsrlist:
    data = pd.read_csv(h, skiprows=9, low_memory=False)
    data = data[data.columns].astype(float, ignore_errors=True)

    datacols = pd.read_csv(h, skiprows=6, low_memory=False)
    datacols = datacols.iloc[0:3]
    datacols = datacols.drop(1)
    datacols = datacols.fillna(method='ffill', axis=1).fillna('-')
    datacols.columns = range(len(datacols.columns))

    ###this is ugly, make something nicer
    mylist = []
    for i in range(len(datacols.columns)):
        mylist.append(datacols[i])
    mylist2 = []
    for i in mylist:
        mystr = i.tolist()
        mylist2.append(mystr)
    mylist3 = []
    for i in mylist2:
        str1 = ' '.join(i)
        mylist3.append(str1)

    data.columns = mylist3

    blklist = list(data.columns.values)
    blklist = blklist[4:]

    cols = list(data.columns)

    data['weektag'] = data['- Type'].map(weeksdict)
    data['seasontag'] = data['- Month'].map(seasonsdict)
    data['daytag'] = data['- Hour'].map(daysdict)

    tagcols = list(data.columns)[-3:]

    for i in tagcols:
        cols.insert(0, i)

    data = data[cols]

    seasonlist = data.seasontag.unique()
    daylist = data.daytag.unique()
    weeklist = data.weektag.unique()
    totals = []
    #read all blocks and add to list
    for c in blklist:
        totalsum = data[c].sum()
        totalmax = data[c].max()
        totalmin = data[c].min()
        sumdump.append(h+":::"+c+":::"+"fullyear"+":::"+str(totalsum)+":::"+str(totalmax)+":::"+str(totalmin))
        for i in seasonlist:
            sumnum = data.loc[data['seasontag'] == i, c].sum()
            maxnum = data.loc[data['seasontag'] == i, c].max()
            minnum = data.loc[data['seasontag'] == i, c].min()
            sumdump.append(h+":::"+c+":::"+i+":::"+str(sumnum)+":::"+str(maxnum)+":::"+str(minnum))

        for i in daylist:
            sumnum = data.loc[data['daytag'] == i, c].sum()
            maxnum = data.loc[data['daytag'] == i, c].max()
            minnum = data.loc[data['daytag'] == i, c].min()
            sumdump.append(h+":::"+c+":::"+i+":::"+str(sumnum)+":::"+str(maxnum)+":::"+str(minnum))

        for i in weeklist:
            sumnum = data.loc[data['weektag'] == i, c].sum()
            maxnum = data.loc[data['weektag'] == i, c].max()
            minnum = data.loc[data['weektag'] == i, c].min()
            sumdump.append(h+":::"+c+":::"+i+":::"+str(sumnum)+":::"+str(maxnum)+":::"+str(minnum))
        

#make df
sumdumpdf = pd.DataFrame([sub.split(":::") for sub in sumdump])
sumdumpdf.columns = ["simname", "blockname", "timeperiod", "sum", "max", "min"]

#export dataframe to csv
fname = "hsr_timeparse.csv"

if os.path.isfile(fname):
    os.remove(fname)
    
with open(fname, 'a') as f:
    sumdumpdf.to_csv(f, header=True)
    
wb = xw.Book(fname)
wb.close()
