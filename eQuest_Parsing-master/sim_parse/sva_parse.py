import glob as gb
import os
import re
import pandas as pd
import xlwings as xw


def sva_parse():
    # varexplore = True
    # if varexplore:

    fname = "__sva_report.csv"
    if os.path.isfile(fname):
        os.remove(fname)

    sysinfostring = "System Type    Altitude Factor   Floor Area (sqft)   Max People   Outside Air Ratio   Cooling Capacity (kBTU/hr)   Sensible (SHR)   Heating Capacity (kBTU/hr)   Cooling EIR (BTU/BTU)   Heating EIR (BTU/BTU)   Heat Pump Supplemental Heat (kBTU/hr)"
    faninfostring = "Sim Name   Report Name   Fan Type   Capacity (CFM)   Diversity Factor (FRAC)   Power Demand (kW)   Fan deltaT (F)   Static Pressure (in w.c.)   Total efficiency   Mechanical Efficiency   Fan Placement   Fan Control   Max Fan Ratio (Frac)   Min Fan Ratio (Frac)"
    zoneinfostring = "Sim Name   Report Name   Zn Name   Supply Flow (CFM)   Exhaust Flow (CFM)   Fan (kW)   Minimum Flow (Frac)   Outside Air Flow (CFM)   Cooling Capacity (kBTU/hr)   Sensible (FRAC)   Extract Rate (kBTU/hr)   Heating Capacity (kBTU/hr)   Addition Rate (kBTU/hr)   Zone Mult"

    for name in gb.glob('./*.sim'):
        svawrite = False
        svalist = []
        rptlist = []
        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            for num, line in enumerate(f_list):
                if "SV-A" in line:
                    svawrite = True
                    currentrpt = line.split('  ')[1]
                if svawrite:
                    svalist.append(line)
                    rptlist.append(currentrpt)
                if "SS-D" in line:
                    svawrite = False

            # make zone df
        syslist = []
        syslist.append(zoneinfostring)
        for num, s in enumerate(svalist):
            if "FACTOR" in s and "PEOPLE" in s:
                syshead = num + 2
            if "Zn" in s:
                syslist.append(name + "   " + rptlist[num] + "   " + s)
        splitlist = []
        for s in syslist:
            splitter = re.split(r'\s{2,}', s)
            splitlist.append(splitter)

        for s in splitlist:
            s[-1] = s[-1].strip().strip('\"').strip('.')

        zonedf = pd.DataFrame(splitlist)
        zonedf = zonedf.astype(float, errors='ignore')

        # make header df
        syslist1 = []
        syslist2 = []
        syslist2.append(sysinfostring)
        syslist1.append(faninfostring)
        for num, s in enumerate(svalist):
            if "FACTOR" in s and "PEOPLE" in s:
                syshead = num + 2
            if ("SUPPLY" in s) and "EXTRACTION" not in s:
                syslist1.append(name + "   " + rptlist[num] + "   " + s)
                syslist2.append(svalist[syshead])
            if ("RETURN" in s) and "EXTRACTION" not in s:
                syslist1.append(name + "   " + rptlist[num] + "   " + s)
                syslist2.append(" ")

        splitlist1 = []
        splitlist2 = []
        for s in syslist1:
            splitter = re.split(r'\s{2,}', s)
            splitlist1.append(splitter)

        for s in splitlist1:
            s[-1] = s[-1].strip().strip('\"').strip('.')

        svadf1 = pd.DataFrame(splitlist1)
        svadf1 = svadf1.astype(float, errors='ignore')

        for s in syslist2:
            splitter = re.split(r'\s{2,}', s)
            splitlist2.append(splitter)

        for s in splitlist2:
            s[-1] = s[-1].strip().strip('\"').strip('.')

        svadf2 = pd.DataFrame(splitlist2)
        svadf2 = svadf2.astype(float, errors='ignore')

        frames = [svadf1, svadf2]
        header = pd.concat(frames, axis=1)

        with open(fname, 'a') as f:
            header.to_csv(f, header=False, index=False)
            zonedf.to_csv(f, header=False, index=False)

        wb = xw.Book(fname)
        wb.close()


if __name__ == "__main__":
    sva_parse()
