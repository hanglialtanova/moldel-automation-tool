'''
inspects all SIM files in folder and returns the total number of unmet hours as well as the
system and zone names for the x highest offenders (default is 10 but can change head(x) as
necessary) for both heating and cooling unmet hours. 

returns csv file in same folder with info above.

'''

import glob as gb
import os
import pandas as pd
import xlwings as xw


def unmet_ssr(numsys=10, write=True):
    fname = "__unmetsummary.csv"
    # numsys = 10
    if os.path.isfile(fname):
        os.remove(fname)

    for name in gb.glob('./*.sim'):
        ssrwrite = False
        ssrstart = False
        rptlist = []
        znlist = []
        zninfolist = []

        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            for num, line in enumerate(f_list):
                if 'HOURS ANY ZONE ABOVE COOLING THROTTLING RANGE' in line:
                    unmetcoolnum = line
                    unmetcoolnum = unmetcoolnum.strip(
                        "           HOURS ANY ZONE ABOVE COOLING THROTTLING RANGE                =")
                if 'HOURS ANY ZONE BELOW HEATING THROTTLING RANGE' in line:
                    unmetheatnum = line
                    unmetheatnum = unmetheatnum.strip(
                        "           HOURS ANY ZONE BELOW HEATING THROTTLING RANGE                =")

                # start scanning ss-r
                if "SS-R" in line:
                    currentrpt = line.split('  ')[1]
                    ssrstart = True
                    ssrwrite = True

                # writevars
                if ssrwrite:
                    if "Zn" in line and "WEATHER FILE" not in line:
                        rptlist.append(currentrpt)
                        modline = line.split('  ')
                        znlist.append(modline[0])
                        zninfolist.append(f_list[num + 1])

                # stop scanning ss-r
                if "SS-L" in line:
                    ssrwrite = False
                    ssrstart = False

        # deal with ssr reports
        znsplitlist = []
        for z in zninfolist:
            splitter = z.split()
            znsplitlist.append(splitter)

        undercoollist = []
        underheatlist = []
        bothlist = []
        znsplitlist = [z for z in znsplitlist if len(z) > 1]

        for z in znsplitlist:
            undercoollist.append(int(z[3]))
            underheatlist.append(int(z[2]))

        for num, line in enumerate(undercoollist):
            both = line + underheatlist[num]
            bothlist.append(both)

        ssrdf = pd.DataFrame([rptlist, znlist, underheatlist, undercoollist, bothlist]).transpose()

        cols = ["Sys Name", "Zone Name", "Underheated Hrs", "Undercooled Hrs", "Sum of Both"]
        ssrdf.columns = cols
        heatsort = ssrdf.sort_values("Underheated Hrs", ascending=False).head(numsys)
        coolsort = ssrdf.sort_values("Undercooled Hrs", ascending=False).head(numsys)
        bothsort = ssrdf.sort_values("Sum of Both", ascending=False).head(numsys)

        # deal with beps unmet hours
        heatsort = heatsort.drop(columns=(['Undercooled Hrs', 'Sum of Both']), axis=1)
        coolsort = coolsort.drop(columns=(['Underheated Hrs', 'Sum of Both']), axis=1)
        bothsort = bothsort.drop(columns=(['Undercooled Hrs', 'Underheated Hrs']), axis=1)
        unmetcoolnum = unmetcoolnum.strip('').strip('\n')
        unmetheatnum = unmetheatnum.strip('').strip('\n')

        coolindex = 'UNMET COOLING HOURS'
        heatindex = 'UNMET HEATING HOURS'
        indexdf = [coolindex, heatindex]
        unmetdf = pd.DataFrame([unmetcoolnum, unmetheatnum])
        unmetdf['Unmet Hr Type'] = indexdf
        unmetdf['Unmet Hr Summary'] = name
        names = unmetdf.columns.tolist()
        names[0] = 'Hours Unmet'
        unmetdf.columns = names
        newcols = ['Unmet Hr Summary', 'Unmet Hr Type', 'Hours Unmet']
        unmetdf = unmetdf[newcols]
        unmetdf = unmetdf.astype(int, errors='ignore')

        # export dataframe to csv
        # fname = "__" + name[2:].strip(".SIM").strip(" - Baseline Design")+"_unmetsummary.csv"
        if write:
            with open(fname, 'a') as f:
                unmetdf.to_csv(f, header=True, index=False)
                heatsort.to_csv(f, header=True, index=False)
                coolsort.to_csv(f, header=True, index=False)
                bothsort.to_csv(f, header=True, index=False)
            wb = xw.Book(fname)
            wb.close()
        return unmetdf


if __name__ == "__main__":
    unmet_ssr()
