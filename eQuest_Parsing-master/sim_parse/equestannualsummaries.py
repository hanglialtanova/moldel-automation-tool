import glob as gb
import os
import warnings
import pandas as pd
import xlwings as xw

warnings.filterwarnings("ignore")

'''

STEPS OF FUNCTION:
READ SIM FILE
MAKE BEPS DATAFRAME
MAKE BEPU DATAFRAME
MAKE ES-E DATAFRAME
TAKE ES-E DATAFRAME AND MERGE VIRTUAL RATE WITH BEPU DATAFRAME
PRINT BEPS DATAFRAME TO CSV
PRINT BEPU/ES-E DATAFRAME TO CSV

'''


def equestannualsummaries(write=True):
    costout = "__cost_out.csv"
    unmetout = "__unmet_hours.csv"
    bepsout = "__beps_out.csv"
    bepuout = "__bepu_out.csv"

    simfiles = gb.glob('./*.sim')

    if os.path.isfile(costout):
        os.remove(costout)
    if os.path.isfile(unmetout):
        os.remove(unmetout)
    if os.path.isfile(bepsout):
        os.remove(bepsout)
    if os.path.isfile(bepuout):
        os.remove(bepuout)

    for name in simfiles:
        with open(name) as f:
            flist = f.readlines()

            #### ES-E PARSE
            esenum = []
            for num, line in enumerate(flist, 0):
                if "ES-E" in line:
                    esenum.append(num)
            eserpt = []
            for i in esenum:
                eserpt.append(flist[i:i + 75])
            meterlist = []
            for a in eserpt:
                replist = []
                for l in a:
                    replist.append(l)
                rate = replist[0]
                rate = rate[44:65].rstrip()
                meters = replist[5]
                meters = meters[18:-2]
                meters = meters.split()
                vratenum = []
                for num, line in enumerate(replist, 0):
                    if "TOTAL" in line and "VIRTUAL" not in line:
                        vratenum.append(num)
                vrate = replist[vratenum[0]]
                vratesplit = vrate.split()
                # vrate = vratesplit[10]
                try:
                    vrate = str(float(vratesplit[11]) / float(vratesplit[1]))
                except:
                    pass
                for i in meters:
                    meterlist.append(i + "," + vrate + "," + rate)
            ese_df = pd.DataFrame([sub.split(",") for sub in meterlist])

            #### BEPS PARSE
            beps_count = []
            for num, line in enumerate(flist, 0):
                if 'BEPS' in line:
                    beps_count.append(num)
                if 'BEPU' in line:
                    numend = num
            numstart = beps_count[0]
            BEPS_rpt = flist[numstart:numend]
            MBTU_str = []
            for line in BEPS_rpt:
                if ('MBTU' in line and 'TOTAL' not in line):
                    MBTU_str.append(line)
            Meter_Index = []
            for line in BEPS_rpt:
                if ('ELECTRICITY' in line or 'NATURAL-GAS' in line or 'CHILLED-WATER' in line or 'STEAM' in line):
                    if 'GENERATE' not in line and 'TOTAL' not in line:
                        Meter_Index.append(line)
            Meter_Index_Split = []
            for line in Meter_Index:
                splitter = line.split()
                Meter_Index_Split.append(splitter)
            Meter_Array = pd.DataFrame(Meter_Index_Split)
            MBTU_list = []
            for line in MBTU_str:
                splitter = line.split()
                MBTU_list.append(splitter)
            beps_df = pd.DataFrame(MBTU_list)
            beps_df.insert(0, column=14, value=0)
            beps_df.insert(0, column=15, value=0)
            beps_df[15] = Meter_Array[0]
            beps_df[14] = Meter_Array[1]
            beps_df.columns = ['METER NAME', 'UTILITY TYPE', 'UNITS', 'LIGHTS', 'TASK LIGHTS', 'MISC EQUIP',
                               'SPACE HEATING', 'SPACE COOLING', 'HEAT REJECT', 'PUMPS & AUX', 'VENT FANS',
                               'REFRIG DISPLAY', 'HT PUMP SUPPLEM', 'DOMEST HOT WTR', 'EXT USAGE', 'TOTAL']
            beps_df.index.name = name
            beps_df = beps_df[:-1]
            beps_df.insert(0, 'RUNNAME', name)
            with open(bepsout, 'a') as f:
                beps_df.to_csv(f, header=True, index=False)

            #### BEPU PARSE
            bepu_count = []
            for num, line in enumerate(flist, 0):
                if 'BEPU' in line:
                    bepu_count.append(num)
                if 'PS-H' in line:
                    numend = num
                    break
            numstart = bepu_count[0]
            bepu_rpt = flist[numstart:numend]
            unit_str = []

            for line in bepu_rpt:
                if ('MBTU' in line or 'KWH' in line or 'THERM' in line and 'TOTAL' not in line):
                    unit_str.append(line)
            Meter_Index = []
            for line in bepu_rpt:
                if ('ELECTRICITY' in line or 'NATURAL-GAS' in line or 'CHILLED-WATER' in line or 'STEAM' in line):
                    if 'GENERATE' not in line and 'TOTAL' not in line:
                        Meter_Index.append(line)
            Meter_Index_Split = []
            for line in Meter_Index:
                splitter = line.split()
                Meter_Index_Split.append(splitter)
            bepu_meterarray = pd.DataFrame(Meter_Index_Split)
            unit_list = []
            for line in unit_str:
                splitter = line.split()
                unit_list.append(splitter)
            bepu_df = pd.DataFrame(unit_list)
            bepu_df.insert(0, column=14, value=0)
            bepu_df.insert(0, column=15, value=0)
            bepu_df[15] = bepu_meterarray[0]
            bepu_df[14] = bepu_meterarray[1]
            bepu_df.columns = ['METERNAME', 'UTILITYTYPE', 'UNITS', 'LIGHTS', 'TASK LIGHTS', 'MISC EQUIP',
                               'SPACE HEATING', 'SPACE COOLING', 'HEAT REJECT', 'PUMPS & AUX', 'VENT FANS',
                               'REFRIG DISPLAY', 'HT PUMP SUPPLEM', 'DOMEST HOT WTR', 'EXT USAGE', 'TOTAL']
            bepu_df.index.name = name
            bepu_df = bepu_df[:-1]

            #### MERGE VRATE AND BEPU IF THERE ARE ANY RATES IN MODEL, OTHERWISE MERGE
            if len(ese_df.index) > 0:
                ese_df.columns = ['METERNAME', 'VRATE', 'RATE']
                ratedf = pd.merge(left=bepu_df, right=ese_df, how='outer')
                ratedf.insert(0, 'RUNNAME', name)
                with open(bepuout, 'a') as f:
                    ratedf.to_csv(f, header=True, index=False)
            else:
                ratedf = bepu_df
                ratedf[name] = name
                with open(bepuout, 'a') as f:
                    ratedf.to_csv(f, header=True, index=False)

                    #### COST PARSE
            costdf = ratedf.convert_objects(convert_numeric=float)
            # costdf = ratedf.astype('float',errors='ignore')
            # costdf = costdf[:-1]
            costlist = []

            counter = 0
            for i in range(len(costdf)):
                # costlist.append(pd.DataFrame(costdf.select_dtypes(exclude=['object']).iloc[i] * costdf.iloc[i].VRATE))
                costlist2 = (pd.DataFrame(costdf.select_dtypes(exclude=['object']).iloc[i] * costdf.iloc[i].VRATE))
                costlist2 = costlist2.transpose()
                costlist2['RUNNAME'] = costdf.iloc[i].RUNNAME
                costlist2['RATE'] = costdf.iloc[i].RATE
                costlist2['METERNAME'] = costdf.iloc[i].METERNAME
                costlist2['UTILITY TYPE'] = costdf.iloc[i].UTILITYTYPE
                cols = costlist2.columns.tolist()
                cols = cols[-4:] + cols[:-4]
                costlist2 = costlist2[cols]
                costlist2 = costlist2.drop('VRATE', axis=1)

                if counter == 0:
                    with open(costout, 'a') as f:
                        costlist2.to_csv(f, header=True, index=False)
                    counter = counter + 1
                else:
                    with open(costout, 'a') as f:
                        costlist2.to_csv(f, header=False, index=False)

                    #### PARSE UNMET HOURS

            for num, line in enumerate(flist, 0):
                if 'HOURS ANY ZONE ABOVE COOLING THROTTLING RANGE' in line:
                    unmetcoolnum = line
                if 'HOURS ANY ZONE BELOW HEATING THROTTLING RANGE' in line:
                    unmetheatnum = line

            unmetcoolnum = unmetcoolnum.split("=")
            unmetheatnum = unmetheatnum.split("=")
            unmetcoolnum = unmetcoolnum[1].split()
            unmetheatnum = unmetheatnum[1].split()

            coolindex = 'UNMET COOLING HOURS'
            heatindex = 'UNMET HEATING HOURS'
            indexdf = [coolindex, heatindex]
            unmetdf = pd.DataFrame([unmetcoolnum, unmetheatnum])
            unmetdf['UNMETTYPE'] = indexdf
            unmetdf['RUNNAME'] = name
            names = unmetdf.columns.tolist()
            names[0] = 'HOURS'
            unmetdf.columns = names
            newcols = ['RUNNAME', 'UNMETTYPE', 'HOURS']
            unmetdf = unmetdf[newcols]
    #            with open (unmetout, 'a') as f:
    #                unmetdf.to_csv(f,header = True, index = False)

    wb = xw.Book(costout)
    wb.close()

    # wb = xw.Book(unmetout)
    # wb.close()

    wb = xw.Book(bepsout)
    wb.close()

    wb = xw.Book(bepuout)
    wb.close()
    return costdf, unmetdf, bepu_df, beps_df


if __name__ == "__main__":
    equestannualsummaries()
