import glob as gb
import os
import pandas as pd
import xlwings as xw


def ssa_parse(simfile='all', write=True):
    # varexplore = True
    # if varexplore:
    ssastart = False
    ssbstart = False
    sslstart = False
    ssaraw = []
    ssaclean = []
    ssbraw = []
    ssbclean = []
    ssarptlistraw = []
    ssarptlistclean = []
    ssbrptlistraw = []
    ssbrptlistclean = []
    namelistraw = []
    namelistclean = []

    sslraw = []
    sslclean = []
    sslrptlistraw = []
    sslrptlistclean = []

    months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

    if simfile == 'all':
        simlist = gb.glob('./*.sim')
    else:
        simlist = [simfile]

    for name in simlist:
        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            for line in f_list:

                # scan ss-a
                if "REPORT- SS-A" in line or "REPORT- SS-B" in line:
                    currentrpt = line[40:-50].strip()
                    ssastart = True
                if ssastart and "SS-C" not in line:
                    ssaraw.append(line)
                    ssarptlistraw.append(currentrpt)
                    namelistraw.append(name)
                if "SS-B" in line:
                    ssastart = False

                # scan ss-b
                if "SS-B" in line:
                    currentrpt = line[40:-50].strip()
                    ssbstart = True
                if ssbstart and "SS-C" not in line:
                    ssbraw.append(line)
                    ssbrptlistraw.append(currentrpt)
                if "SS-C" in line:
                    ssbstart = False

                # scan ss-l
                if "SS-L" in line:
                    currentrpt = line[40:-50].strip()
                    sslstart = True
                if sslstart and "SS-N" not in line:
                    sslraw.append(line)
                    sslrptlistraw.append(currentrpt)
                if "SS-N" in line:
                    sslstart = False

            # strip out everything except months
            for num, s in enumerate(ssaraw):
                if any(month in s for month in months) and "THERM    " not in ssarptlistraw[num]:
                    ssaclean.append(s)
                    ssarptlistclean.append(ssarptlistraw[num])
                    thisname = namelistraw[num]
                    thisname = thisname.replace(" - Baseline Design.SIM", "").replace('.\\', '')
                    namelistclean.append(thisname)

            for num, s in enumerate(ssbraw):
                if any(month in s for month in months) and "THERM    " not in ssbrptlistraw[num]:
                    ssbclean.append(s)
                    ssbrptlistclean.append(ssbrptlistraw[num])

            for num, s in enumerate(sslraw):
                if any(month in s for month in months) and "THERM    " not in sslrptlistraw[num]:
                    sslclean.append(s)
                    sslrptlistclean.append(sslrptlistraw[num])

    # split and parse lists
    ssaparse = []
    for s in ssaclean:
        splitter = s.split()
        ssaparse.append(splitter)

    ssbparse = []
    for s in ssbclean:
        splitter = s.split()
        ssbparse.append(splitter)

    sslparse = []
    for s in sslclean:
        splitter = s.split()
        fansum = float(splitter[1]) + float(splitter[2]) + float(splitter[3]) + float(splitter[4])
        sslparse.append(fansum)

    # make DFs
    ssadf = pd.DataFrame(ssaparse, ssarptlistclean)
    ssadf = ssadf.drop([2, 3, 4, 5, 8, 9, 10, 11], axis=1)
    ssacols = ["MONTH", "COOLING ENERGY (MBTU)", "MAX COOLING LOAD (KBTU/HR)", "HEATING ENERGY (MBTU",
               "MAX HEATING LOAD (KBTU/HR)", "ELEC ENERGY (KWH)", "MAX ELEC LOAD (KW)"]
    ssadf.columns = ssacols

    ssbdf = pd.DataFrame(ssbparse, ssbrptlistclean)
    ssbcols = ["MONTH", "ZONE COOLING (MBTU)", "MAX COOLING BY ZONE COILS (KBTU/HR)", "ZONE HEATING (MBTU)",
               "MAX HEATING BY ZONE COILS (KBTU/HR)", "BASEBOARD HEATING (MBTU)", "MAX BASEBOARD HEATING (KBTU/HR)",
               "PREHEAT COIL ENERGY (MBTU)", "MAX PREHEAT COIL ENERGY (KBTU/HR)"]
    ssbdf.columns = ssbcols
    ssbdf = ssbdf.drop('MONTH', axis=1)

    ssldf = pd.DataFrame(sslparse, sslrptlistclean)
    ssldf.columns = ['Fan Energy (kWh)']

    concdf = [ssadf, ssbdf, ssldf]

    totaldf = pd.concat(concdf, axis=1)
    totaldf['Sim Name'] = namelistclean

    if write:
        # export dataframe to csv
        fname = "__sys_reports.csv"

        if os.path.isfile(fname):
            os.remove(fname)

        with open(fname, 'a') as f:
            totaldf.to_csv(f, header=True)

        wb = xw.Book(fname)
        wb.close()

    return totaldf


if __name__ == "__main__":
    ssa_parse()
