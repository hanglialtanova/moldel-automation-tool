'''
USER ENTRY: DESIRED REPORTS IN THE FOLLOWING SAMPLE FORMAT: 
reports = ["ES-D", "SV-A", "BEPS"]. CASE-SENSITIVE. 
'''


reports = ["ES-D"]
#standard LEED Reports: ["BEPS", "BEPU", "ES-D"]

import glob as gb
import os
import sys
import shutil

#os.chdir(r"C:\Users\msweeney\Desktop\MS EM Tools\Python\Python Scripts\Sample SIMs")

simfiles = gb.glob('./*.sim')

directory = "Report Outputs"

if os.path.exists(directory):
    shutil.rmtree(directory)

os.makedirs(directory)
                     
for name in simfiles:
    
    with open(name) as f:
        f_list = f.readlines()
        rptstart = []
        for num, line in enumerate(f_list,0):
            for r in reports:                
                    if r in line:
                        rptstart.append(int(num)-2)      

        for r in rptstart:           
            lines = 0
            scan = f_list[r+3:(r+1000)]

            while lines is 0:  
                for num, line in enumerate(scan):
                    rptlen = []
                    if "REPORT" in line:
                        rptlen.append(num)
                        lines = lines + 1
                        break
                        
            list = (f_list[r:(r+rptlen[0]+1)])
            rptno = 0
            file = name + " USER OUTPUTS.SIM"            
            with open(os.path.join(directory,file), "a") as output:
                for l in list:
                    output.write(l)


