import pandas as pd
import numpy as np
import re
import glob as gb
import os
import csv
import sys
import xlwings as xw
import inp_parse.objects as obj


def lvd_totals(write=True):
    rptname = "LV-D"
    nextrpt = "LV-E"

    rptstart = 0
    fname = "__envelope_summary.csv"
    if os.path.isfile(fname):
        os.remove(fname)
    for num, name in enumerate(gb.glob('./*.sim')):

        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            rpt_count = []

            # make list out of whole report
            for n, line in enumerate(f_list, 0):
                if rptname in line:
                    rpt_count.append(n)
                if nextrpt in line:
                    numend = n
            numstart = rpt_count[rptstart]
            rpt = f_list[numstart:numend]
            var_str = []

            # pull out only summary
            summarylist = []
            summstart = False
            for line in rpt:

                line = line.strip()

                if "AVERAGE" in line:
                    summstart = True
                if summstart:
                    summarylist.append(line)
                if "BUILDING" in line:
                    summstart = False

            summlistsplit = []
            for line in summarylist:
                splitter = re.split(r'\s{2,}', line)
                summlistsplit.append(splitter)

            cols = ['FACADE TYPE', 'AVG U WINDOWS', 'AVG U WALLS', 'AVG U WALLS+WINDOWS', 'WINDOW AREA', 'WALL AREA',
                    'WINDOW+WALL AREA']
            summlistdf = pd.DataFrame(summlistsplit).dropna(axis=0, how='any', thresh=2).dropna(axis=1, how='all').iloc[
                         3:]

            summlistdf.columns = cols
            if write:
                with open(fname, 'a') as f:
                    summlistdf.to_csv(f, header=True, index=False)

                    wb = xw.Book(fname)
                    wb.close()

            return summlistdf


def customroofs(write=True):
    rptname = "LV-D"
    nextrpt = "LV-E"

    rptstart = 0
    lvddf = pd.DataFrame()
    for num, name in enumerate(gb.glob('./*.sim')):

        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            rpt_count = []

        # make list out of whole report
        for n, line in enumerate(f_list, 0):
            if rptname in line:
                rpt_count.append(n)
            if nextrpt in line:
                numend = n
        numstart = rpt_count[rptstart]
        rpt = f_list[numstart:numend]

        split = []
        for r in rpt:
            splitter = re.split(r'\s{2,}', r)
            split.append(splitter)

        splitnew = []

        splitnew = split

        splitnew2 = []
        for s in splitnew:
            if "ROOF" in s[0].upper() and len(s) > 7:
                splitnew2.append(s)

        lvddf = pd.DataFrame(splitnew2)
        lvddf = lvddf.drop([1, 2, 3, 4, 5, 8], axis=1)

    if write:
        sname = "__roof_surfaces.csv"
        if os.path.isfile(sname):
            os.remove(sname)
        with open(sname, 'w') as f:
            lvddf.to_csv(f)
            wb = xw.Book(sname)
            wb.close()
    return lvddf


def underhangs(write=True, underhang='underhang'):
    rptname = "LV-D"
    nextrpt = "LV-E"

    rptstart = 0
    lvddf = pd.DataFrame()
    for num, name in enumerate(gb.glob('./*.sim')):

        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            rpt_count = []

        # make list out of whole report
        for n, line in enumerate(f_list, 0):
            if rptname in line:
                rpt_count.append(n)
            if nextrpt in line:
                numend = n
        numstart = rpt_count[rptstart]
        rpt = f_list[numstart:numend]

        split = []
        for r in rpt:
            splitter = re.split(r'\s{2,}', r)
            split.append(splitter)

        splitnew = []

        splitnew = split

        splitnew2 = []
        for s in splitnew:
            if underhang in s[0]:
                splitnew2.append(s)

        #        splitnew3 = []

        lvddf = pd.DataFrame(splitnew2)

        lvddf = lvddf.drop([1, 2, 3, 4, 5, 8], axis=1)

    sname = "__floor_surfaces.csv"
    if write:
        if os.path.isfile(sname):
            os.remove(sname)
            with open(sname, 'w') as f:
                lvddf.to_csv(f)
                wb = xw.Book(sname)
                wb.close()
    return lvddf


def lvd_detailed(simfile="All", write=True):
    '''returns csv of all detailed wall entries
    '''

    rptname = "LV-D"
    nextrpt = "LV-E"

    rptstart = 0

    if simfile == "All":
        sims = gb.glob('./*.sim')
    else:
        sims = [simfile]

    for num, name in enumerate(sims):

        fname = "__envelope_detailed.csv"
        if os.path.isfile(fname):
            os.remove(fname)

        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()

        # make list out of whole report
        rpt_count = []
        for n, line in enumerate(f_list, 0):
            if rptname in line:
                rpt_count.append(n)
            if nextrpt in line:
                numend = n
                break
        numstart = rpt_count[rptstart]
        rpt = f_list[numstart:numend]

        summarylist = rpt
        summlistsplit = []
        for line in summarylist:
            splitter = re.split(r'\s{2,}', line)
            summlistsplit.append(splitter)

        cols = ['Wall Name', 'WIN-U', 'WIN-AREA', 'WALL-U', 'WALL-AREA', 'WALL+WIN-U', 'WALL+WIN AREA', 'AZIMUTH']
        summlistdf = pd.DataFrame(summlistsplit).dropna(axis=0, how='any', thresh=2).dropna(axis=1, how='all').iloc[3:]
        summlistdf = summlistdf.dropna(axis=0, how='any').drop(axis=1, columns=8)
        summlistdf.columns = cols

        if write:
            with open(fname, 'a') as f:
                summlistdf.to_csv(f, header=True, index=False)

                wb = xw.Book(fname)
                wb.close()
        return summlistdf


def spacecorrelate(df, inpfile):
    '''takes sim file and inpfile and correlates to 
    '''

    flist = obj.openinp(inpfile)
    walls = obj.get(flist, 'EXTERIOR-WALL')
    dfwalls = df['Wall Name'].values.tolist()
    uwalls = obj.get(flist, 'UNDERGROUND-WALL')
    walls.update(uwalls)

    spacedict = {}
    for key, value in walls.items():
        for d in dfwalls:
            if d == key:
                spacedict[d] = value['parent']

    spacedf = pd.DataFrame(spacedict, index=[0]).transpose()
    spacedf.columns = ['Corresponding Space']

    mergedf = df.merge(right=spacedf, how='left', left_on='Wall Name', right_on=spacedf.index)
    return mergedf


def lvd_detailed_with_space_correlate(simfile, inpfile, fname="lvd_with_space_correlate.csv", write=True):
    '''produces detailed lvd report with correlation
    for each space'''

    df = lvd_detailed(simfile=simfile, write=False)
    dfspc = spacecorrelate(df, inpfile)
    dfspc = dfspc.set_index('Wall Name')
    if write:
        dfspc.to_csv(fname)
    return dfspc


if __name__ == "__main__":
    lvd_totals()
    customroofs()
    underhangs()
