import pandas as pd
import glob as gb
import xlwings as xw
import re
import os
import inp_parse.objects as obj

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

idx = pd.IndexSlice


def psf_parse(writecsv=True, leedpivot=False):
    elecvars = ["KWH ", "MAX KW "]
    gasvars = ["THERM ", "MAX THERM/HR "]
    districtvars = ["MBTU ", "MAX MBTU/HR "]

    psfwrite = False
    psfstart = False

    currentrptlist = []
    varlist = []
    namelist = []
    monthlist = []
    for name in gb.glob('./*.sim'):
        with open(name, encoding="Latin1") as f:
            f_list = f.readlines()
            for num, line in enumerate(f_list):
                # start scanning ps-f

                if "PS-F" in line:
                    currentrpt = line.split('  ')[2].strip()
                    psfstart = True
                if psfstart:
                    psfwrite = True

                # writevars
                if psfwrite:
                    if (elecvars[0] in line or gasvars[0] in line or districtvars[
                        0] in line) and "TRANSFORMER" not in line:
                        simname = name.strip(" - Baseline Design.SIM").strip("\\")
                        newline = line.replace(".", ". ")
                        varlist.append(newline)

                        month = f_list[num - 1].strip()
                        if len(month) < 3:
                            month = 'TOTAL'
                        #                        print (month)
                        monthlist.append(month)
                        currentrptlist.append(currentrpt)
                        namelist.append(simname)

                    if (elecvars[1] in line or gasvars[1] in line or districtvars[
                        1] in line) and "TRANSFORMER" not in line:
                        varlist.append(line)
                        month = f_list[num - 2].strip()
                        if len(month) < 3:
                            month = 'TOTAL'
                        monthlist.append(month)
                        currentrptlist.append(currentrpt)
                        namelist.append(simname)
                        psfwrite = False

                # stop scanning ps-f
                if "BEPS" in line:
                    psfwrite = False
                    psfstart = False

                    # split consumption/demand values to lists
    varlist2 = []
    for a in varlist:
        a.replace('MAX_KW', 'MAX_KW')
        a.replace('MAX THERM/HR', 'MAX_THERM_HR')
        varlist2.append(
            a.replace('MAX KW', 'MAX_KW').replace('MAX THERM/HR', 'MAX_THERM_HR').replace('MAX_MBTU_HR', 'MAX_MBTU'))

    varlistsplit = []
    for a in varlist2:
        if "MAX MBTU/HR" in a:
            splitter = re.split(r'\s{2,}', a)
        else:
            splitter = re.split(r'\s{1,}', a)

        varlistsplit.append(splitter)

    units = []
    for i in varlistsplit:
        if "MAX" in i[0]:
            units.append('DEMAND')
        else:
            units.append('CONSUMPTION')

    df = pd.DataFrame(varlistsplit)

    df['METERNAME'] = currentrptlist
    df['SIMNAME'] = namelist
    df['MONTH'] = monthlist
    df['UNITS'] = units

    df.columns = ['UNITS', 'LIGHTS', 'TASK LIGHTS', 'MISC EQUIP', 'SPACE HEATING',
                  'SPACE COOLING', 'HEAT REJECT', 'PUMPS & AUX', 'VENT FANS',
                  'REFRIG DISPLAY', 'HT PUMP SUPPLEM', 'DOMEST HOT WTR', 'EXT USAGE',
                  'TOTAL', 'BLANK', 'METERNAME', 'SIM NAME', 'MONTH', 'ENERGY TYPE']
    #    return df
    cols = ['SIM NAME', 'METERNAME', 'ENERGY TYPE', 'UNITS', 'MONTH', 'LIGHTS', 'TASK LIGHTS', 'MISC EQUIP',
            'SPACE HEATING',
            'SPACE COOLING', 'HEAT REJECT', 'PUMPS & AUX', 'VENT FANS',
            'REFRIG DISPLAY', 'HT PUMP SUPPLEM', 'DOMEST HOT WTR', 'EXT USAGE',
            'TOTAL']

    df = df[cols]
    df.set_index(['SIM NAME', 'METERNAME', 'ENERGY TYPE', 'UNITS', 'MONTH'], drop=True, inplace=True)
    df = df.astype(float, errors='ignore')

    if leedpivot:
        df = pivot_for_leed(df)

    if writecsv:
        psf_fname = "__psf_reports.csv"
        df.to_csv(psf_fname)
        wb = xw.Book(psf_fname)
        wb.close()
    return df


def pivot_for_leed(df):
    '''
    constructs a pivoted df with demand/consumption for each meter. 
    optional for psf_parse.
    '''

    a = psf_parse()
    cons = psf_filter(a, timeagg='YEAR', by='meters').T
    dem = psf_filter(a, energytype='demand', timeagg='YEAR', by='meters').T
    comb = cons.join(dem)
    return comb


def getrates():
    '''kind of a work in progres... not really sure how to accomplish this.'''
    simfiles = gb.glob('./*.sim')
    with open(simfiles[0], 'r') as f:
        flist = f.readlines()

    write = False
    alldict = {}
    for f in flist:

        if 'REPORT- ES-E Summary of Utility-Rate' in f:
            write = True
            meterdict = {}
            report = f.split("Utility-Rate:")[1].split("WEATHER FILE")[0].strip()

        if write:
            if "RESOURCE" in f:
                resource = f.split("RESOURCE:")[1].split('DEMAND')[0].strip()
            # meterdict['resource'] = resource
            if "METERS:" in f:

                meters = f.replace("METERS:", "").strip().split('  ')
                for m in meters:
                    # print (m)
                    alldict[m] = resource
                    alldict[m] = report

        if "ES-F Block-Charges" in f:
            write = False

    return alldict


def psf_filter(df, by="units", energytype='consumption', timeagg='month', dropmeter=None, dropendusetotal=True):
    '''
    takes df created from from psf_parse and filters.
    args:
    by: "units" or "meters". 
    energytype = 'consumption' or 'demand'
    timeagg  'month' or 'year'
    dropmeter = list of meter names to drop (i.e. dummy meters etc.)
    
    '''
    if dropendusetotal:
        df = df.drop('TOTAL', axis=1)

    # drop meters
    if type(dropmeter) != list:
        dropmeter = [dropmeter]

    for meter in dropmeter:
        try:
            df = df.drop(meter, level=1)
        except:
            pass

    # filter by time
    if timeagg.upper() == 'MONTH':
        df = df.drop('TOTAL', level=4).drop('DEMAND', level=3)

    if timeagg.upper() == 'YEAR':
        df = df.loc[idx[:, :, :, :, "TOTAL"], :]

    # unit/energy filters
    df = df = df.loc[idx[:, :, energytype.upper(), :, :, ], :]

    if by.upper() == 'UNITS':
        df = df.groupby(['SIM NAME', 'ENERGY TYPE', 'UNITS', 'MONTH'], sort=False).sum()

    if by.upper() == 'METERS':
        df = df.groupby(['SIM NAME', 'METERNAME', 'UNITS', 'MONTH'], sort=False).sum()

    return df


def df_filterlist(df, filterzeroes=True):
    if "METERNAME" in df.index.names:
        levelvals = df.index.get_level_values(1)
        indexlist = df.index.get_level_values(1).unique().tolist()

        dflist = []
        for unit in indexlist:
            tempdf = pd.DataFrame()
            tempdf = df.loc[idx[:, unit, :, :, ], :]
            if filterzeroes:
                tempdf = tempdf.loc[:, (tempdf != 0).any(axis=0)]

            dflist.append(tempdf)

    else:
        levelvals = df.index.get_level_values(2)
        indexlist = df.index.get_level_values(2).unique().tolist()

        dflist = []
        for unit in indexlist:
            tempdf = pd.DataFrame()
            tempdf = df.loc[idx[:, :, unit, :], :]
            if filterzeroes:
                tempdf = tempdf.loc[:, (tempdf != 0).any(axis=0)]

            dflist.append(tempdf)

    return dflist


def make_barplots(dflist):
    for d in dflist:
        ax = d.plot(kind='bar', stacked='true', figsize=(20, 10))

        title = d.index.values[0][0] + ", " + d.index.values[0][1] + " by End-Use (" + d.index.values[0][2] + ")"
        plt.title(title)
        ax.set_ylabel(d.index.values[0][2])
        ax.set_xlabel("Monthly End-Use Breakdown")

        tick = mtick.StrMethodFormatter('{x:,.0f}')
        ax.yaxis.set_major_formatter(tick)

        monthlabels = [item.get_text() for item in ax.get_xticklabels()]
        monthlabels = [x[-5:-1] for x in monthlabels]

        ax.set_xticklabels(monthlabels)

        plt.show()
        plt.clf()


if __name__ == "__main__":
    psf_parse()
