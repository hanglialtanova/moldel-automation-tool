'''
alternative to parsing hsr file. hsr file doesn't have units, and sim file doesn't have blocks - but
blocks shouldn't matter because there's a 1:1 mapping between object and block.

-also a better sim table parser.

'''

import re
import pandas as pd

class RptList(dict): 
    def __repr__(self):
        return 'Available Reports:\n' + '\n'.join([key for key in self.keys()])
    __str__ = __repr__
   
   
class RptDf:
    '''container for customized report dataframes, can pass on various metadata 
    into it (zone/volume, etc) and transformation methods'''
    def __init__(self, name, df):
        self.df = df
        self.name = name

def sim_hrly_to_dict(file):
    '''handle hourly report parsing'''
    with open(file) as f:
        fstr = f.read()
    hrly_text = re.findall(r'(HOURLY REPORT- .*?\f)',fstr,re.DOTALL)
    hrlydict = {}
    for r in hrly_text:
        #need to handle hourly report
        report = re.findall("HOURLY REPORT-.*WEATHER", r)[0]
        report = report.replace("REPORT- ","").replace("WEATHER","").strip()
        top = re.split(r'\s{2,}', report)[0] 
        rptdata = r.split("\n")[2:]
        #populate dictionaries
        if top not in hrlydict:
            hrlydict[top] = rptdata
        else:
            hrlydict[top] = hrlydict[top] + rptdata

    return hrlydict
def sim_to_dict(file):
    '''parses sim file, returns dictionary of tables and sub tables'''
    with open(file) as f:
        fstr = f.read()
        
    rpt_text = re.findall(r'(REPORT.*?\f)',fstr, re.DOTALL) # works for all but last report

    #handle tables
    rptdict = {}
    for r in rpt_text:
        report = re.findall("REPORT-.*WEATHER", r)[0]
        report = report.replace("REPORT- ","").replace("WEATHER","").strip()
        # handle special parse cases
        
        if "DESIGN DAY" in report:
            top = re.split(r'\s{2,}', report)[0] + ' (DESIGN DAY)'
            try:
                bottom = re.split(r'\s{2,}', report)[1]
                if bottom == "":
                    bottom = 'None'
            except:
                bottom = 'None'
            
        elif "LS-J Daylight Illuminance Frequency" in report:
            top = "LS-J Daylight Illuminance Frequency"
            bottom = report.replace((top + " "),"")

        elif "LS-M Daylight Illuminance Ref Pnt 1" in report:
            top = "LS-M Daylight Illuminance Ref Pnt 1"
            bottom = report.replace((top + " "),"")    
        
        elif "SS-P Heating Performance Summary of" in report:
            top = "SS-P Heating Performance Summary of"
            bottom = report.replace((top + " "),"")  
            
        elif "SS-P Cooling Performance Summary of" in report:
            top = "SS-P Heating Performance Summary of"
            bottom = report.replace((top + " "),"")          
            
        # handle rest of cases
        else:
            top = re.split(r'\s{2,}', report)[0]
            try:
                bottom = re.split(r'\s{2,}', report)[1]
                if bottom == "":
                    bottom = 'None'
            except:
                bottom = 'None'
                
        rptdata = r.split("\n")[3:]

        #populate dictionaries
        if top not in rptdict:
            rptdict[top] = {bottom:rptdata}
        
        else:
            if bottom in rptdict[top]:
                rptdict[top][bottom] = rptdict[top][bottom] + rptdata
            else:
                rptdict[top].update({bottom: rptdata})


        


    table = RptList(rptdict)
    
    return table

    

def lsb_df_parse(sim, norm = False):
    '''returns dictionary of spaces and resultant load dataframes'''
    lsb_dict = {}
    for key, value in sim['LS-B Space Peak Load Components'].items():
        rpt_text = re.findall(r'(WALL CONDUCTION.*?--------)','\n'.join(value), re.DOTALL)
        rpt_text = [r.split("\n") for r in rpt_text][0][:-1]
        rpt_text = [r.replace("LIGHT     TO SPACE", "LIGHT TO SPACE").replace("PROCESS   TO SPACE","PROCESS TO SPACE") for r in rpt_text]
        rpt_text = [re.split(r'\s{2,}', r.strip()) for r in rpt_text]
        
        #to_df
        rpt_df = pd.DataFrame(rpt_text)
        rpt_df.index = rpt_df.iloc[:,0]
        rpt_df = rpt_df.iloc[:,1:]
        rpt_df = rpt_df.astype(float)
        
        area = float(re.split(r'\s{2,}',re.findall('FLOOR  AREA.*?SQFT', ''.join(value))[0])[2])
        volume = float(re.split(r'\s{2,}',re.findall('VOLUME.*?CUFT',''.join(value))[0])[1])
        
        if norm:
            rpt_df = rpt_df.apply(lambda x: (x * 1000 / area))
            rpt_df.columns = pd.MultiIndex.from_tuples([('COOLING', 'SENSIBLE', 'BTU/H/SF'), 
                                                        ('COOLING', 'SENSIBLE', 'W/SF'), 
                                                        ('COOLING', 'LATENT', 'BTU/H/SF'), 
                                                        ('COOLING', 'LATENT', 'W/SF'), 
                                                        ('HEATING', 'SENSIBLE', 'BTU/H/SF'), 
                                                        ('HEATING', 'SENSIBLE', 'W/SF')])
        else:
            rpt_df.columns = rpt_df.columns = pd.MultiIndex.from_tuples([('COOLING', 'SENSIBLE', 'KBTU/H'), ('COOLING', 'SENSIBLE', 'KW'), ('COOLING', 'LATENT', 'KBTU/H'), ('COOLING', 'LATENT', 'KW'), ('HEATING', 'SENSIBLE', 'KBTU/H'), ('HEATING', 'SENSIBLE', 'KW')])
        rpt_df.index.name = key + " Loads"
    
        #to_class
        rpt_df = RptDf(rpt_df.index.name, rpt_df)
        rpt_df.area = area
        rpt_df.volume = volume

        rpt_df.val = value
        lsb_dict[key] = rpt_df
        
    return lsb_dict
    
'''
#example load barplot:

simfile  = (r'P:\_Projects\180000\181238-000\D-Design Mgmt\Calc\Energy\Energy Model\eQuest\proposed\chop prop - Baseline Design.SIM')
sim = sim_parse.sim_to_dict(simfile)
load_dict = sim_parse.lsb_df_parse(sim, norm=True)

dflist = []
for key, val in load_dict.items():
    dflist.append(val.df.iloc[:,4]) #### this integer represents each type of load (cooling sensible / latent / heating etc)
    
concatdf  = pd.concat(dflist,axis=1)
concatdf.columns = [d.index.name for d in dflist]
concatdf = concatdf[concatdf.sum().sort_values(ascending=True).index]

data = [go.Bar(y=concatdf.columns,
               x=concatdf.loc[d,:].values,
               name = d,
              orientation = 'h'
              )
        for d in concatdf.index]

layout = go.Layout(barmode='stack',
                  height=30000,width=1000, margin=dict(l=300,r=0,t=0,b=0))
fig = go.Figure(data=data, layout=layout)
py.iplot(fig)

'''