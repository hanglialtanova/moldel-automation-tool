import pandas as pd
import numpy as np
import re
import glob as gb
import os
import csv
import sys
import xlwings as xw

###### USER INPUT: REPORT NAME, NEXT REPORT NAME, VARIABLES IN STRINGS TO PARSE - UNCOMMENT ANY SAMPLES BUT COMMENT UNUSED ONES: ####

rptname = "SS-P Cooling"
nextrpt = "SS-G"

#uvars = ["NORTH ", "NORTH-EAST ", "SOUTH-EAST ","SOUTH-WEST ","ROOF ", "ALL WALLS ", "WALLS+ROOFS ", "UNDERGRND ", "BUILDING"]
#uvars = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTAL"]
uvars = ["YR "]


#fname = (rptname+" reports.csv")
#if os.path.isfile(fname):
#    os.remove(fname)

simfiles = gb.glob('./*.sim')
write = False
sysname = []
syspeaks = []

write = False
simname = gb.glob('./*.sim')
with open('City Point Base - Baseline Design.SIM', encoding="Latin1") as f:
    f_list = f.readlines()
for num, f in enumerate(f_list):
    if 'SS-P Cooling' in f:
        write = True
        systag = f[43:70].strip()
        sysname.append(systag)
        
    if "YR" in f and write:
        syspeaks.append(f_list[num+1])
        write = False
    


unitload_kbtu = []
energyuse_kwh = []
compressor_kwh = []
fanenergy_kwh= []


for f in syspeaks:
    splitter = f.split()
    unitload_kbtu.append(splitter[1])
    energyuse_kwh.append(splitter[2])
    compressor_kwh.append(splitter[3])
    fanenergy_kwh.append(splitter[4])

        
compdf = pd.DataFrame([sysname, unitload_kbtu, energyuse_kwh, compressor_kwh, fanenergy_kwh]).transpose()

compdf.columns = ['sysname', 'unitload_kbtu', 'energyuse_kwh', 'compressor_kwh', 'fanenergy_kwh']
        
#compdf['PEAKCOP'] = compdf.loc[compdf.unitload_kbtu + compdf.energyuse_kwh]

compdf.to_csv('compressor_peaks.csv')

#
#wb = xw.Book(fname)
#wb.close()



