import glob as gb
import os
import pandas as pd
import xlwings as xw

#os.chdir(r"C:\Users\msweeney\Desktop\MS EM Tools\Python\Python Scripts\Working\Sample SIMs\SIM Comprehensive")

#os.chdir(r"P:\_Projects\170000\170363-000\D-Design Mgmt\Calc\Energy\eQuest Model\Nov 2017 Models\Prop - VRF")

simfiles = gb.glob('./*.sim')

fname = ("meter_reports.csv")
if os.path.isfile(fname):
    os.remove(fname)

for name in simfiles:
    with open(name) as f:
        flist = f.readlines()

        #### ES-E PARSE: ASSIGN EACH METER TO VIRTUAL RATE

        esenum = []
        for num, line in enumerate(flist, 0):
            if "ES-E" in line:
                esenum.append(num)

        eserpt = []
        for i in esenum:
            eserpt.append(flist[i:i+75])

        meterlist = []
        for a in eserpt:
            replist = []
            for l in a:
                replist.append(l)
            rate = replist[0]
            rate = rate[44:65].rstrip()
            meters = replist[5]
            meters = meters[18:-2]
            meters = meters.split()

            vratenum = []
            for num, line in enumerate(replist, 0):
                if "TOTAL" in line and "VIRTUAL" not in line:
                    vratenum.append(num)

            vrate = replist[vratenum[0]]
            vratesplit = vrate.split()
            vrate = vratesplit[10]
    
            for i in meters:
                meterlist.append(i+","+vrate+","+rate)
    
        ese_df = pd.DataFrame([sub.split(",") for sub in meterlist])
    
        #### PS-F PARSE: MAKE PS-F DF FOR EACH METER       

        rpt_count = []
        for num, line in enumerate(flist,0):
            if "PS-F" in line:
                rpt_count.append(num)
            if "BEPS" in line:
                numend = num 
        numstart = rpt_count[0]
        rpt = flist[numstart:numend]
                
        psf_vars = ["KWH ", "THERM ", "MBTU "]
        var_str = []
        current_rpt = rpt[0]
        current_rpt_list = []
        for v in psf_vars:            
            for line in rpt:
                if "PS-F" in line:
                    current_rpt = line                   
                if v in line and "VIRTUAL" not in line:
                    var_str.append(line)  
                    current_rpt_list.append(current_rpt)
  
            rpt_list = []
            for line in var_str:
                splitter = line.split()
                rpt_list.append(splitter)
            
            rpt_total = []

            counter = 0
            for i in rpt_list:
                counter = counter + 1
                if counter == 12:
                    rpt_total.append(i)
                    counter = 0
        
            rpt_total_tag = []  
            counter1 = 0
            for i in current_rpt_list:
                counter1 = counter1 + 1
                if counter1 == 12:
                    rpt_total_tag.append(i)
                    counter1 = 0          
  
        rpt_total_tag_2 = []
        for i in rpt_total_tag:
            string = i[40:70].strip(' ')
            string = string[0:4]
            rpt_total_tag_2.append(string)
            
        mdf_df = pd.DataFrame(rpt_total)
        mdf_df_tag = pd.DataFrame(rpt_total_tag_2)
        
        psf_df = pd.concat([mdf_df, mdf_df_tag], axis=1)

        psf_df.columns = ['UNITS', 'LIGHTS', 'TASK LIGHTS', 'MISC EQUIP', 'SPACE HEATING', 'SPACE COOLING', 'HEAT REJECT', 'PUMPS & AUX', 'VENT FANS', 'REFRIG DISPLAY', 'HT PUMP SUPPLEM', 'DOMEST HOT WTR', 'EXT USAGE', 'TOTAL', 'METER']
        cols = psf_df.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        psf_df = psf_df[cols]
        ese_df.columns = ['METER', 'VRATE', 'RATE']
        

        mergedf = pd.merge(left=psf_df,right=ese_df,how='outer')
            
        
       
        #with open(fname, 'a') as f:
        #    psf_df.to_csv(f, header = True)    
        #    ese_df.to_csv(f, header = True) 

#wb = xw.Book(fname)
#wb.close()


        











