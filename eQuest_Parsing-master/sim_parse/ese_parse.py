'''
parses es-e in sim file
'''

import glob as gb
import os
import pandas as pd
import xlwings as xw


def ese_parse(writecsv=True, cons_demand_only=True):
    '''returns ese sim values.
    ese consumption and peaks are different from
    ps-f reports in that they are summed by meter
    and also that they do not necessarily have
    the first day of the month as a starting
    point.

    '''

    simfiles = gb.glob('./*.sim')
    rptname = "ES-E Summary of Utility-Rate"
    monthlist = ['JAN',
                 'FEB',
                 'MAR',
                 'APR',
                 'MAY',
                 'JUN',
                 'JUL',
                 'AUG',
                 'SEP',
                 'OCT',
                 'NOV',
                 'DEC']

    for name in simfiles:
        simname = name.strip(".SIM").strip("\\")
        rptlist = []
        report = 'None'
        write = False
        with open(name) as f:
            flist = f.readlines()

        for f in flist:
            if 'REPORT- ' in f and 'WEATHER FILE-' in f:
                report = f.split("REPORT- ")[1].split("WEATHER FILE")[0].strip()
            if "RESOURCE" in f:
                unit = f.split("/")[-1].strip()
            if "$/UNIT" in f:
                write = True
            if rptname in report and "===" not in f and write:
                reportshort = report.split(":")[1].strip()

                monthname = f[1:5].strip()
                if any(monthname in t for t in monthlist):
                    splitter = f.split()
                    if len(splitter) == 14:
                        splitter.insert(0, unit)
                        splitter.insert(0, reportshort)
                        splitter.insert(0, simname)
                        rptlist.append(splitter)
            if "TOTAL" in f:
                write = False

    columns = ["SIM NAME",
               "RATE NAME",
               'UNIT',
               "MONTH",
               "METERED ENERGY",
               "BILLING ENERGY",
               "METERED DEMAND",
               "BILLING DEMAND",
               "ENERGY CHARGE ($)",
               "DEMAND CHARGE ($)",
               "ENERGY CST ADJ ($)",
               "TAXES ($)",
               "SURCHRG ($)",
               "FIXED CHARGE ($)",
               "MINIMUM CHARGE ($)",
               "VIRTUAL UNIT RATE ($/UNIT)",
               "TOTAL CHARGE ($)"]

    esedf = pd.DataFrame(rptlist)
    esedf = esedf.astype(float, errors='ignore')
    esedf.columns = columns
    # esedf = esedf.set_index("SIM NAME")

    if cons_demand_only:
        esedf = esedf[['SIM NAME',
                       'RATE NAME',
                       'UNIT',
                       'MONTH',
                       'METERED ENERGY',
                       'METERED DEMAND']]

    if writecsv:
        fname = ("ese_meter_reports.csv")
        if os.path.isfile(fname):
            os.remove(fname)

        esedf.to_csv(fname, index=False)
        wb = xw.Book(fname)
        wb.close()

    return esedf


if __name__ == "__main__":
    ese_parse()
