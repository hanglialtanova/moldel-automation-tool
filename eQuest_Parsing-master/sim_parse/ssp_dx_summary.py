





import pandas as pd
import numpy as np
import re
import glob as gb
import os
import csv
import sys
import xlwings as xw

###### USER INPUT: REPORT NAME, NEXT REPORT NAME, VARIABLES IN STRINGS TO PARSE - UNCOMMENT ANY SAMPLES BUT COMMENT UNUSED ONES: ####

def annualsum(df):

    return df


def dx_summary_monthly(simfile, condtype = 'Cooling', allsims=True,annualsum=False,cop_pivot=True):

    if condtype.upper() == 'COOLING':    
        rptname = "SS-P Cooling"
        nextrpt = "SS-G Zone Loads"
        
    if condtype.upper() == 'HEATING':    
        rptname = "SS-P Heating"
        nextrpt = "SS-P Cooling"
     
        
        
    months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
    
    if allsims:
        simfiles = gb.glob('./*.sim')
    write = False
    syscons = []
    
    with open(simfile, encoding="Latin1") as f:
        f_list = f.readlines()
        
        
    for num, f in enumerate(f_list):
        if rptname in f:
            write = True
            systag = f.split(rptname)[1].replace("Performance Summary of ","").split("WEATHER")[0].strip()
            
        if nextrpt in f:
            write = False
                   
        if write:
            for m in months:
                if m in f:
                    splitter = f.split()
                    unitload_mmbtu = splitter[2]
                    energyuse_kwh = splitter[3]
                    compressor_kwh = splitter[4]
                    fanenergy_kwh = splitter[5]                   
                    month = m
                    
                    syscons.append([systag, month, unitload_mmbtu, energyuse_kwh, compressor_kwh, fanenergy_kwh])
                    
                    
            if "YR" in f or nextrpt in f:
                write = False
                
    compdf = pd.DataFrame(syscons)
    compdf.columns = ['sysname', 'month', 'unitload_mmbtu', 'energyuse_kwh', 'compressor_kwh', 'fanenergy_kwh']

    compdf.iloc[:,2:] = compdf.iloc[:,2:].astype(float, errors='raise')
    compdf = compdf[(compdf != 0).all(1)]
   
    compdf['EffectiveCOP'] =  ((compdf['unitload_mmbtu'] * 1000) / 3.412) / compdf['energyuse_kwh']
#   
    if condtype.upper() == 'HEATING':
        compdf['EffectiveCOP'] = compdf['EffectiveCOP'].abs()

    if annualsum:
        compdf = compdf.groupby('sysname').sum()
        compdf = compdf.drop('month', axis=1)
        compdf['EffectiveCOP'] = ((compdf['unitload_mmbtu'] * 1000) / 3.412) / compdf['energyuse_kwh']
    
    if cop_pivot and not annualsum:
        compdf = compdf.pivot(index='sysname',columns='month',values='EffectiveCOP').transpose()
        compdf = compdf.reindex(months)
        
    if not cop_pivot and not annualsum:
        monthdict = {'JAN': 1,
                     'FEB': 2,
                     'MAR': 3,
                     'APR': 4,
                     'MAY': 5,
                     'JUN': 6,
                     'JUL': 7,
                     'AUG': 8,
                     'SEP': 9,
                     'OCT': 10,
                     'NOV': 11,
                     'DEC': 12}

        compdf['monthtag'] = compdf['month'].apply(lambda a: monthdict[a])
        
    return compdf
 
    













def plant_summary_monthly(simfile, allsims=True, condtype='Cooling', annualsum=False,cop_pivot=True):
      
   
    rptname = "PS-H Loads and Energy Usage"
    nextrpt = "HOURLY REPORT-"
   
    months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
    
    if allsims:
        simfiles = gb.glob('./*.sim')
    write = False
    syscons = []
    
    with open(simfile, encoding="Latin1") as f:
        f_list = f.readlines()
        
        
    for num, f in enumerate(f_list):
        if rptname in f:
            print (f)
            write = True
            systag = f.split(rptname)[1].replace("Performance Summary of ","").split("WEATHER")[0].strip()
            systag = systag.replace('for','').strip()
        if nextrpt in f:
            write = False
                   
        if write:
            for m in months:
                if m in f and "PRIMARY" not in f:
                    splitter = f.split()
#                    print (splitter)
                    unitload_mmbtu = splitter[2]
                    energyuse_kwh = splitter[3]
                    compressor_kwh = splitter[4]
                    fanenergy_kwh = splitter[5]                   
                    month = m
#                    print (systag)
                    syscons.append([systag, month, unitload_mmbtu, energyuse_kwh, compressor_kwh, fanenergy_kwh])
                    
                    
        if "YR" in f or nextrpt in f:
            write = False


    compdf = pd.DataFrame(syscons)
#    return compdf

    return compdf    
    compdf.columns = ['sysname', 'month', 'unitload_mmbtu', 'energyuse_kwh', 'compressor_kwh', 'fanenergy_kwh']

    compdf.iloc[:,2:] = compdf.iloc[:,2:].astype(float, errors='raise')
    compdf = compdf[(compdf != 0).all(1)]
   
    compdf['EffectiveCOP'] =  ((compdf['unitload_mmbtu'] * 1000) / 3.412) / compdf['energyuse_kwh']
#   
    if condtype.upper() == 'HEATING':
        compdf['EffectiveCOP'] = compdf['EffectiveCOP'].abs()

    if annualsum:
        compdf = compdf.groupby('sysname').sum()
        compdf = compdf.drop('month', axis=1)
        compdf['EffectiveCOP'] = ((compdf['unitload_mmbtu'] * 1000) / 3.412) / compdf['energyuse_kwh']
    
    if cop_pivot and not annualsum:
        compdf = compdf.pivot(index='sysname',columns='month',values='EffectiveCOP').transpose()
        compdf = compdf.reindex(months)

    return compdf
 
